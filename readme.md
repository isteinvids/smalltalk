## TODO: 

* webcam chat
* email verification

## Work in progress:

* none :(

## Done:

* fixing and testing cloud
* contacts/friends (WIP)
* profile pictures
* password encryption on both client and server
* do login authentication and account registration
* auto login in for client
* when not in development, auto-update the files
* uses alertify instead of the ugly default alert windows
