var UserSession = require("./usersession.js");
var utils = require("./util.js");
var fs = require("file-system");

commands = {};
commands["setuser"] = handleSetUser;
commands["getuser"] = handleGetUser;
commands["pub"] = handlePublicKey;
commands["uploadFile"] = handleFileUpload;
commands["downloadFile"] = handleFileDownload;
commands["deleteFile"] = handleFileDelete;
commands["addOrRemFriend"] = handleAddOrRemoveFriend;
commands["session"] = handleSessionRequest;

needsIndexUpdate = {};

module.exports = {
  handleCommand: function(usersession, params, postdata, uuid) {
    if (params["cmd"] && commands[params["cmd"]]) {
      if (params["cmd"] !== "pub" && params["cmd"] !== "uploadFile") {
        postdata = usersession.rsaEncryption.decryptWithOwnKey(postdata);
      }
      toret = commands[params["cmd"]](usersession, params, postdata, uuid);
      if (params["cmd"] !== "pub" && params["cmd"] !== "downloadFile") {
        toret = usersession.rsaEncryption.encryptWithKey(toret, usersession.clientPublicKey);
      }
      return toret;
    }
    return undefined;
  }
};

function handleSessionRequest(usersession, params, postdata) {
  return JSON.stringify(usersession.writeToJson());  
}

function handleAddOrRemoveFriend(usersession, params, postdata) {
  if (params["action"] && postdata) {
    var chat = require("./chat.js");
    act = params["action"];
    user = postdata;
    if (user === usersession.username) {
      return handleSessionRequest(usersession);
    }
    if (act === "remove") {
      index = usersession.contacts.indexOf(user);
      if (index > -1) {
        usersession.contacts.splice(index, 1);
      }
      index = usersession.requests.indexOf(user);
      if (index > -1) {
        usersession.requests.splice(index, 1);
      }
    }
    if (act === "confirm") {
      othersession = chat.getUserSession(user); //the user that's being confirmed
      if (usersession.requests.indexOf(user) !== -1 && usersession.contacts.indexOf(user) === -1) {
        index = usersession.requests.indexOf(user);
        usersession.requests.splice(index, 1);
        usersession.contacts.push(user);
      }
      if (othersession.contacts.indexOf(usersession.username) === -1) {
          othersession.contacts.push(usersession.username);
      }
    }
    if (act === "request") {
      othersession = chat.getUserSession(user);
      if (othersession.contacts.indexOf(usersession.username) === -1) {
        index = othersession.requests.indexOf(usersession.username);
        if (index === -1) {
          othersession.requests.push(usersession.username);
        }
      }
      othersession.saveToFile();
    }

    usersession.saveToFile();
  }
  return handleSessionRequest(usersession);
}

function handleSetUser(usersession, params, postdata) {
  if (postdata) {
    setuserto = postdata;
    if (setuserto === username) {
      return "cannot set current user to your own username"; 
    }
    usersession.currentUser = setuserto;
    return "200";
  }
  return "invalid POST parameters";
}

function handleGetUser(usersession, params, postdata) {
  if (usersession.currentUser) {
    return usersession.currentUser;
  }
  return "null";
}

function handlePublicKey(usersession, params, postdata) {
  usersession.clientPublicKey = postdata;
  return usersession.rsaEncryption.getPublicKey();
}

function handleFileUpload(usersession, params, postdata, uuid) {
  if (!params["to"]) {
    return "don't know where to upload to";
  }
  var chat = require("./chat.js");
  file = "cloud/" + usersession.username + "/" + params["to"];
  fs.writeFileSync(file, postdata, "hex");
  chat.needsCloudUpdate(uuid, true);
  return "200";
}

function handleFileDelete(usersession, params, postdata, uuid) {
  if (!params["del"]) {
    return "don't know what to delete";
  }
  var chat = require("./chat.js");
  file = "cloud/" + usersession.username + "/" + params["del"];
  if (fs.existsSync(file)) {
    console.log("deleting " + file);
    fs.unlinkSync(file);
    parent = file.substring(0, file.lastIndexOf("/"));
  }
  chat.needsCloudUpdate(uuid, true);
  return "200";
}

function handleFileDownload(usersession, params, postdata) {
  if (!params["file"]) {
    return "no file parameter";
  }
  dir = params["file"];
  if (dir.indexOf("../") > -1 && dir.indexOf("cloud/" + usersession.username + "/") !== 0) {
    return "invalid file" + dir;
  }
  if (!fs.existsSync(dir)) {
    return "file does not exist " + dir;
  }
  ret = fs.readFileSync(dir, "hex");
  if (ret) {
    return ret;
  }
  return "could not read file, for some reason";
}
