var fs = require('fs');

module.exports = {
  logMessage: function(message, user1, user2) {
    time = Date.now();
    message = time + ":" + message + "\n";
    file = "logs/"+user2 + "-" + user1 + ".log";
    if (user1 < user2) {
      file = "logs/"+user1 + "-" + user2 + ".log";
    }
    if (!fs.existsSync("./logs")) {
      fs.mkdirSync("./logs");
    }
    fs.appendFile(file, message, function (err) {
      if (err) {
        console.error("couldn't log to file (" + file + "), exception: "+err);
      }
    });
  },
  logLogin: function(username, uuid, ip) {
    time = Date.now();
    message = time + ": " + username + " has logged in from " + ip + " and given UUID: " + uuid;
    file = "logs/_login.log";
    if (!fs.existsSync("./logs")) {
      fs.mkdirSync("./logs");
    }
    fs.appendFile(file, message, function (err) {
      if (err) {
        console.error("couldn't log to file (" + file + "), exception: "+err);
      }
    });
  }
};