console.log("Starting server...");

var rsaenc = require("./rsaenc.js");
var util = require("./util.js");
var RSA = require("./rsaenc.js");
var wait = require("wait.for");
var fs = require("file-system");

ip = "127.0.0.1";
port = "4446";

//process parameters
process.argv.forEach(function (val, index, array) {
  if(val.indexOf("--") === 0 && index + 1 < array.length){
    key = val.substring(2, val.length);
    value = array[index+1];
    if(key === "serverIp") {
      ip = value;
      console.log("setting ip to " + value);
    }
    if(key === "port") {
      port = value;
      console.log("setting port to " + value);
    }
  }
});

pages = {};
sessions = {};

module.exports.addPage = addPage;
module.exports.getSessions = getSessions;

function getSessions(){
  return sessions;
}

function addPage(name, func){
  console.log("Adding page " + name + "...");
  pages[name] = func;
}

addPage("login", handleLogin);
addPage("register", handleRegister);

var http = require("http");
var chat = require("./chat.js");
var qs = require("querystring");

var server = http.createServer(function (request, response) {
  if (request.url.indexOf("/cloud/") === 0 && request.url.indexOf("../") === -1) {
    file = "." + request.url;
    file = file.substring(2, file.length);

    p1 = file.substring(6, file.length);
    p1 = p1.substring(0, p1.indexOf("/"));
    console.log(p1);
    p2 = file.substring(file.indexOf("/") + 1, file.length);
    p2 = p2.substring(p2.indexOf("/") + 1, file.length);

    file = "./cloud/" + p1 + "/public/" + p2;
    if (file.indexOf("?") !== -1) {
      file = file.substring(0, file.lastIndexOf("?"));
    }
    file = util.decode(file);
    response.writeHead(200, {"Content-Type": "text/plain"});
    if (!fs.existsSync(file) || !fs.lstatSync(file).isFile()) {
      response.end("404 file not found (" + file + ")");
      return;
    }
    response.end(fs.fs.readFileSync(file));
  } else {
    var data = "";
    request.on("data", function(data_) {
      data += data_;
    }).on("end", function() {
      wait.launchFiber(handleReq, request, response, data);
    });

  }
  
});
server.listen(port, ip);

console.log("Server running at http:/" + ip + ":" + port + "/");

function handleLogin(request, response, url, params, postdata) {
  if (postdata) {
    server = new RSA();
    server.parsePrivateKey(fs.readFileSync("privatekey", "utf8"));
    postdata = server.decryptWithOwnKey(postdata);

    jsontext = JSON.parse(postdata);

    username = jsontext["username"];
    password = util.encryptmd5(jsontext["password"]);

    file = "accounts/" + username + ".dat";

    if (!fs.existsSync(file)) {
      response.end("Invalid username or password");
      return;
    }
    accountjson = JSON.parse(fs.readFileSync(file, "utf8"));

    if (accountjson.username !== username || accountjson.password !== password || !accountjson.email) {
      response.end("Invalid username or password");
      return;
    }
    if (!accountjson.verified) {
      // response.end("The email for this account is not verified.");
      // return;
    }

    uuid = util.generateUUID();
    sessions[uuid] = username;
    response.end(uuid);
    return;
  }
  response.end("invalid params");
}

function handleRegister(request, response, url, params, postdata) {
  if (postdata) {
    server = new RSA();
    server.parsePrivateKey(fs.readFileSync("privatekey", "utf8"));
    postdata = server.decryptWithOwnKey(postdata);

    jsontext = JSON.parse(postdata);

    email = jsontext["email"];
    username = jsontext["username"];
    password = util.encryptmd5(jsontext["password"]);

    file = "accounts/" + username + ".dat";
    
    if (username.length <= 4) {
      response.end("Username is too short");
      return;
    }
    
    if (username.length > 20) {
      response.end("Username is too long");
      return;
    }

    if (/[^a-zA-Z0-9_]/.test(username)) {
      response.end("Username has invalid characters");
      return;
    }

    if (fs.existsSync(file)) {
      response.end("Username or email is taken");
      return;
    }

    files = fs.readdirSync("./accounts/");
    for (i = 0; i < files.length; i++) {
      tempemail = JSON.parse(fs.readFileSync("./accounts/" + files[i], "utf8"))["email"];
      if (tempemail === email) {
        response.end("Username or email is taken");
        return;
      }
    }
    //authenticate user/pass here
    towrite = {"email": email, "username" : username, "password" : password, "verified" : false};
    fs.writeFileSync(file, JSON.stringify(towrite, null, 4), "utf8");
    response.end("200");
    return;
  }
  response.end("invalid params");
}

function handleReq(request, response, data) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  try {
    urlRequested = request.url;
    urlName = urlRequested.substring(1, urlRequested.length);
    params = {};
    if(urlRequested.indexOf("?") > -1) {
      urlName = urlRequested.substring(1, urlRequested.indexOf("?"));
      paramString = urlRequested.substring(urlRequested.indexOf("?") + 1, urlRequested.length);
      paramArr = paramString.split("&");
      for(i = 0; i < paramArr.length; i++) {
        if(paramArr[i].indexOf("=") === -1){
          response.end("invalid args");
          return;
        }
        keyVal = util.split(paramArr[i], "=", 1);
        params[keyVal[0]] = util.decode(keyVal[1]);
      }
    }

    if(pages[urlName]){
      pages[urlName](request, response, urlName, params, data);
    } else {
      response.end("invalid url requested");
    }
  } catch (ex) {
    response.end("an error occurred, " + ex);
    throw ex;
  }
}
