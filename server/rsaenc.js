var cryptico = require('./cryptico.browser.js');

var RSAkey;
var publicKey;
var privateKey;

function RSA() {
}

RSA.prototype.generate = function() {
  util = require("./util.js");
  pass = util.generateString(50);
  this.privateKey = cryptico.generateRSAKey(pass, 1024);
  this.publicKey = cryptico.publicKeyString(this.privateKey);
}

RSA.prototype.getPublicKey = function() {
  return this.publicKey;
}

//encrypts with some key
RSA.prototype.encryptWithKey = function(message, thePublicKey) {
  return cryptico.encrypt(message, thePublicKey).cipher;
}

//encrypts with our public key
RSA.prototype.encryptWithOwnKey = function(message) {
  return cryptico.encrypt(message, this.publicKey).cipher;
}

//decrypts with our private key
RSA.prototype.decryptWithOwnKey = function(encryptedMessage) {
  return cryptico.decrypt(encryptedMessage, this.privateKey).plaintext;
}

RSA.prototype.parsePrivateKey = function(json) {
  this.privateKey = cryptico.RSAKey.parse(json);
  this.publicKey = cryptico.publicKeyString(this.privateKey);
}

RSA.prototype.exportPrivateKeyToJson = function() {
  return JSON.stringify(this.privateKey);
}

module.exports = RSA;
