var commandhandler = require("./commandhandler.js");
var UserSession = require("./usersession.js");
var server = require("./server.js");
var logger = require("./logger.js");
var RSA = require("./rsaenc.js");
var util = require("./util.js");
var fs = require("file-system");
var wait = require("wait.for");

server.addPage("write", handleWrite);
server.addPage("command", handleCommand);
server.addPage("messages", handleMessages);
server.addPage("setuser", handleCurrentUser);
server.addPage("cloudindex", handleCloudIndex);

needsIndexUpdate = {};
sessions = server.getSessions();
tosend = {};
users = {}; // load from file? TODO: make the key uuid, not username

module.exports = {
  needsCloudUpdate: function(uuid, val) {
    needsIndexUpdate[uuid] = val;
  },
  getUserSession: function(username) {
    return getUserSession(username);
  }
};

function getUserSession(username) {
  if (users[username]) {
    return users[username];
  }
  return initUser(username);
}

function initUser(username) {
  toret = util.loadUserSession(username);
  return toret;
}

function handleCloudIndex(request, response, url, params, postdata) {
  if (params["session"] && sessions[params["session"]]) {
    uuid = params["session"];
    username = sessions[params["session"]];
    if (!users[username]) {
      users[username] = initUser(username);
    }
    usersession = users[username];

    needsUpdate = false;
    if (needsIndexUpdate[uuid] === undefined || needsIndexUpdate[uuid] === true) {
      needsUpdate = true;
    }
    if (!needsUpdate) {
      setTimeout(function() {
        wait.launchFiber(handleCloudIndex, request, response, url, params, postdata);
      }, 500);
      return;
    }
    console.log("updating client index");
    needsIndexUpdate[uuid] = false;
    dir = "cloud/" + usersession.username + "/";
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    json = JSON.stringify(util.readdir(dir), null, 4);
    tosendstr = usersession.rsaEncryption.encryptWithKey(json, usersession.clientPublicKey);
    response.end(tosendstr);
  }
  response.end("invalid session");
}

function handleCommand(request, response, url, params, postdata){
  if (params["session"] && sessions[params["session"]]) {
    uuid = params["session"];
    username = sessions[params["session"]];
    if (!users[username]) {
      users[username] = initUser(username);
    }
    //
    ret = commandhandler.handleCommand(users[username], params, postdata, uuid);
    if (ret) {
      response.end(ret);
    } else {
      response.end("invalid command");
    }
    users[username].saveToFile();
    return;
  }
  response.end("invalid session");
}

function handleCurrentUser(request, response, url, params) {
  if (params["session"] && sessions[params["session"]]) {
    uuid = params["session"];
    username = sessions[params["session"]];
    if (!users[username]) {
      users[username] = initUser(username);
    }
    if (params["to"]) {
      setuserto = params["to"];
      if (setuserto === username) {
        response.end("cannot set current user to your own username");
        return; 
      }
      users[username].currentUser = setuserto;

      response.end("200");
    } else {
      if (users[username].currentUser) {
        response.end(users[username].currentUser);
      } else {
        response.end("null");
      }
    }
    users[username].saveToFile();
    return;
  }
  response.end("invalid session");
}

function handleWrite(request, response, url, params, postdata) {
  if (params["session"] && sessions[params["session"]] && postdata) {
    uuid = params["session"];
    username = sessions[params["session"]];
    message = postdata;
    if (!users[username]) {
      users[username] = initUser(username);
    }
    message = users[username].rsaEncryption.decryptWithOwnKey(message);
    if (!users[username].currentUser) {
      response.end("currentUser not set");
      return;
    }
    currUser = users[username].currentUser;
    currUsersSession = getUserSession(currUser);

    if (!tosend[username]) {
      tosend[username] = [];
    }
    if (users[username].contacts.indexOf(currUser) === -1 || currUsersSession.contacts.indexOf(username) === -1) {
      console.log(username + " is not a friend of " + currUser);
      // tosend[username].push("center:This user has not accepted your friend request yet");
      response.end("200");
      return;
    }
    if (!tosend[currUser]) {
      tosend[currUser] = [];
    }
    
    tosend[currUser].push(username + ":" + message);
    tosend[username].push(username + ":" + message);

    logger.logMessage(username + ":" + message, username, currUser);

    users[username].saveToFile();
    response.end("200");
    return;
  }
  response.end("invalid session");
}

function handleMessages(request, response, url, params) {
  if (params["session"] && sessions[params["session"]]) {
    uuid = params["session"];
    username = sessions[uuid];
    if (!users[username]) {
      users[username] = initUser(username);
    }
    currUser = users[username].currentUser;
    tosendstr = "";
    flag = false;
    if (!tosend[username]) {
      flag = true;
    } else {
      arr = tosend[username];
      first = true;
      for(i = 0; i < arr.length; i++) {
        if (!first) {
          tosendstr += "\r";
        }
        tosendstr += arr[i];
        arr.splice(i, 1);
        i--;
        first = false;
      }
      if (first === true){
        flag = true;
      }
    }
    if (flag) {
      setTimeout(function() {
        handleMessages(request, response, url, params);
      }, 500);
      return;
    }
    users[username].saveToFile();
    tosendstr = users[username].rsaEncryption.encryptWithKey(tosendstr, users[username].clientPublicKey);
    response.end(tosendstr);
    return;
  }
  response.end("invalid session");
}
