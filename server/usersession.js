var fs = require('fs');
var RSA = require("./rsaenc.js");


function UserSession(username) {
  this.username = username;
  this.rsaEncryption = new RSA();
  this.rsaEncryption.generate();
  this.clientPublicKey = null;
  this.contacts = [];
  this.requests = [];
  this.blocked = [];
  this.currentUser = "null";
}

UserSession.prototype.saveToFile = function() {
  if (!fs.existsSync("./users/")) {
    fs.mkdirSync("./users/");
  }
  fs.writeFile("./users/"+this.username+".dat", JSON.stringify(this.writeToJson()), function(err) {
    if(err) {
      console.log("error saving file: " + this.username + ", " + err);
    }
  });
}

UserSession.prototype.writeToJson = function(){
  toret = {};
  toret["username"] = this.username;
  toret["status"] = "this is a status for " + this.username;
  toret["contacts"] = this.contacts;
  toret["requests"] = this.requests;
  toret["blocked"] = this.blocked;
  toret["currentUser"] = this.currentUser;
  return toret;
}

UserSession.prototype.readFromJson = function(jsonObj){
  if (jsonObj["currentUser"]) {
    this.currentUser = jsonObj["currentUser"];
  }
  if (jsonObj["contacts"]) {
    this.contacts = jsonObj["contacts"];
  }
  if (jsonObj["requests"]) {
    this.requests = jsonObj["requests"];
  }
  if (jsonObj["blocked"]) {
    this.blocked = jsonObj["blocked"];
  }
}

module.exports = UserSession;
