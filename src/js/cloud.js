fs = require("file-system");
util = require("./js/util.js");

cloudIntervalId = NaN;
cloudIndex = undefined;
transferInProgress = false;
indexInProgress = false;
localfiles = null;
deleted = [];

function fixfile(file) {
    tmp = fixfile1(file);
    tmp = tmp.substring(tmp.indexOf("/") + 1, tmp.length);
    tmp = tmp.substring(tmp.indexOf("/") + 1, tmp.length);
    return tmp;
}

function fixfile2(file) {
    tmp = file.substring(file.indexOf("/") + 1, file.length);
    tmp = tmp.substring(tmp.indexOf("/") + 1, tmp.length);
    return tmp;
}

function fixfile1(file) {
    return file.substring(datapath.length + 1, file.length);
}

function cloudHandler() {
    if (!loggedIn) {
        stopCloudInterval();
        return;
    }
    if (cloudIndex && fs.existsSync(datapath + "/cloud/" + sessionUsername + "/")) {
        localfilesTemp = util.readdir(datapath + "/cloud/" + sessionUsername + "/");
        handleFiles(localfiles, function(file, props) {
            if (!isincloud(localfilesTemp, file)) {
                deleted.push(file);
                to = fixfile(file);
                console.log("0: "+file+", "+to);
                if (fs.existsSync(file)) {
                    fs.unlinkSync(file);
                }
                if (!file.endsWith("Thumbs.db")) {
                    deleteCloudFile(to);
                }
            }
        });
        handleFiles(localfilesTemp, function(file, props) {
            if (!isincloud(cloudIndex, fixfile1(file)) && !file.endsWith("Thumbs.db")) {
                to = fixfile(file);
                uploadCloudFile(file, to);
            }
        });
        localfiles = localfilesTemp;
    }
    downloadIndex();
}

function startCloud() {
    cloudIntervalId = setInterval(cloudHandler, 1000);
}

function stopCloudInterval() {
    clearInterval(cloudIntervalId);
}

function downloadIndex() {
    if (transferInProgress) {
        return;
    }
    if (indexInProgress) {
        if (cloudIndex) {
            handleFiles(cloudIndex, downloadCloudFile);
        }
        return;
    }
    indexInProgress = true;
    $.ajax({
        url: webserver + "cloudindex?session=" + sessionUUID,
        type: 'GET',
        error: function(jqXHR, textStatus, errorThrown) {
            indexInProgress = false;
            alertify.error("could not connect to server: " + errorThrown);
        },
        success: function(result) {
            if (!sessionRSA) return;
            result = sessionRSA.decryptWithOwnKey(result);
            deleted = [];
            try {
                cloudIndex = JSON.parse(result);
                handleFiles(cloudIndex, downloadCloudFile);
            } catch (exception) {
                alertify.error("could not parse " + result);
            }
            indexInProgress = false;
        }
    });
}

function deleteCloudFile(del) {
    filecontents = fs.readFileSync(file, "hex");
    $.ajax({
        url: webserver + "command?session=" + sessionUUID + "&cmd=deleteFile&del=" + del,
        type: 'GET',
        error: function(jqXHR, textStatus, errorThrown) {
          alertify.error("could not connect to server: " + errorThrown);
      },
      success: function(result) {
        if (!sessionRSA) return;
        result = sessionRSA.decryptWithOwnKey(result);
    }
});
}

function uploadCloudFile(file, to) {
    if (file.endsWith("Thumbs.db")) {
        return;
    }
    filecontents = fs.readFileSync(file, "hex");
    transferInProgress = true;
    $.ajax({
        url: webserver + "command?session=" + sessionUUID + "&cmd=uploadFile&to=" + to,
        type: 'POST',
        data: filecontents,
        error: function(jqXHR, textStatus, errorThrown) {
            transferInProgress = false;
            alertify.error("could not connect to server: " + errorThrown);
        },
        success: function(result) {
            if (!sessionRSA) return;
            result = sessionRSA.decryptWithOwnKey(result);
            transferInProgress = false;
        }
    });
}

function downloadCloudFile(file, properties) {
    if (deleted.indexOf(file) > -1){
        console.log("file has been deleted, so we're ignoring the file download: " + file);
        return;
    }
    if (file.endsWith("Thumbs.db")) {
        return;
    }
    util.readmd5((datapath + "/" + file).split("\\").join("/"), function(err, localMD5){
        nfile = (datapath + "/" + file).split("\\").join("/");
        downloadFile = true;
        remoteModTime = properties.mtime;
        if (localMD5 && !fs.existsSync(nfile)) {
            console.log(localMD5 + "??? - " + nfile);
            return;
        }
        if (localMD5) {
            localModTime = fs.lstatSync(nfile).mtime / 1000 | 0;
            if (localMD5 === properties.md5) {
                downloadFile = false;
            } else {
                console.log("checking " + nfile + ", " + localModTime+" == "+remoteModTime);
                if (localModTime === remoteModTime) {
                    downloadFile = true;
                } else if (localModTime > remoteModTime) { // local file is newer, UPLOAD
                    to = fixfile(nfile);
                    uploadCloudFile(nfile, to);
                    downloadFile = false;
                } else if (localModTime < remoteModTime) { // remote file is newer, DOWNLOAD
                    downloadFile = true;
                }
            }
        }
        if (downloadFile && !transferInProgress) {
            console.log("downloading : " + properties.md5 + ", " + localMD5);
            transferInProgress = true;
            $.ajax({
                url: webserver + "command?session=" + sessionUUID + "&cmd=downloadFile&file=" + file,
                type: 'GET',
                error: function(jqXHR, textStatus, errorThrown) {
                    transferInProgress = false;
                    alertify.error("could not connect to server: " + errorThrown);
                },
                success: function(result) {
                    nfile2 = (datapath + "/" + file).split("\\").join("/");
                    try{
                        fs.writeFileSync(nfile2, result, "hex");
                    } catch (ex) {
                        console.log("couldn't write file " + nfile2 + ", error : " + ex);
                    }
                    transferInProgress = false;
                }
            });
        }
    });
}

function handleFiles(files, callback){
    for (file in files) {
        if(typeof files[file].md5 === "string") {
            if (callback) {
                callback(file, files[file]);
            }
        } else {
            handleFiles(files[file], callback);
        }
    }
}

function isincloud(files, thefile) {
  val = false;
  handleFiles(files, function(file, props) {
    if (file === thefile) {
      val = true;
  }
  return;
});
  return val;
}
