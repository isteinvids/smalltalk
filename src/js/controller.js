var RSA = require("./js/rsaenc.js");
var utils = require("./js/util.js");
var fs = require("file-system");

var webserver = "http://yofreke.com:4446/";
if (debug) {
  var webserver = "http://127.0.0.1:4446/";
}

cloudIntervalId = undefined;
currentUser = undefined;
usersession = undefined;
serverPub = null;
messages = {};
init = false;

registerPage("login", function() {
  if (!init) {
    autologin();
    init = true;
  }
});

registerPage("profile", function() {
  this.currentUser = "null";
  updateFriends();
  updateRequests();
  setProfile(sessionUsername, usersession.status);
});

registerPage("chat", function() {
  updateFriends();
  updateTextBox();
  setProfile(sessionUsername, usersession.status);
});

function updateRequests() {
  if (usersession) {
    if (typeof addRequest === "function" && typeof clearRequests === "function") {
      clearRequests();
      for (i = 0; i < usersession.requests.length; i++) {
        addRequest(usersession.requests[i]);
      }
      updateProfilePics();
    }
  }
}

function handleRegister(theForm) {
  mainElement().innerHTML = "Registering...<br/><br/><img src='loading.gif'/>";
  email = theForm.inputRegEmail.value;
  user = theForm.inputRegUsername.value;
  pass = utils.encryptmd5(theForm.inputRegPassword.value);
  jsondata = "{\"username\": \"" + user + "\", \"email\": \"" + email + "\", \"password\": \"" + pass + "\"}";

  client = new RSA();
  jsondata = client.encryptWithKey(jsondata, fs.readFileSync("publickey.pub", "utf8"));

  $.ajax({
    url: webserver + "register?v=" + version,
    type: 'POST',
    data: jsondata,
    error: function(jqXHR, textStatus, errorThrown) {
      openPage("login");
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      alertify.alert("register result: " + result);
      openPage("login");
    }
  });

  return false;
}

function autologin() {
  lastloginDir = datapath + sep + "lastlogin.dat";
  if (!fs.existsSync(lastloginDir)) {
    return;
  }
  jsondata = fs.readFileSync(lastloginDir, "utf8");
  
  mainElement().innerHTML = "Logging in...<br/><br/><img src='loading.gif'/>";

  loggedIn = true;
  sessionRSA = new RSA();
  sessionRSA.generate();

  $.ajax({
    url: webserver + "login?v=" + version,
    type: 'POST',
    data: jsondata,
    error: function(jqXHR, textStatus, errorThrown) {
      logout();
      alertify.alert("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      validUUID = (/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(result));
      if (validUUID) {
        sessionUUID = result;
        sendPublicKey();
        startCloud();
        return;
      }
      logout();
      forgetme();
      alertify.alert("login result: " + result);
    }
  });
  return false;
}

function handleLogin(theForm) {
  sessionUsername = theForm.inputUsername.value;
  pass = utils.encryptmd5(theForm.inputPassword.value);
  mainElement().innerHTML = "Logging in...<br/><br/><img src='loading.gif'/>";
  loggedIn = true;
  sessionRSA = new RSA();
  sessionRSA.generate();

  jsondata = "{\"username\": \"" + sessionUsername + "\", \"password\": \"" + pass + "\"}";//, \"version\": \"" + version + "\"}";

  client = new RSA();
  jsondata = client.encryptWithKey(jsondata, fs.readFileSync("publickey.pub", "utf8"));

  $.ajax({
    url: webserver + "login?v=" + version,
    type: 'POST',
    data: jsondata,
    error: function(jqXHR, textStatus, errorThrown) {
      logout();
      alertify.alert("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      validUUID = (/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(result));
      if (validUUID) {
        if (theForm && theForm.remember.checked) {
          lastloginDir = datapath + sep + "lastlogin.dat";
          if (fs.existsSync(lastloginDir)) {
            fs.unlinkSync(lastloginDir);
          }
          fs.writeFileSync(lastloginDir, jsondata, "utf8");
          alert(lastloginDir);
        }
        sessionUUID = result;
        sendPublicKey();
        startCloud();
        return;
      }
      logout();
      alertify.alert("login result: " + result);
    }
  });
  return false;
}

function sendPublicKey() {
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=pub",
    type: 'POST',
    data: sessionRSA.getPublicKey(),
    error: function(jqXHR, textStatus, errorThrown) {
      openPage("login");
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      serverPub = result;
      finishLogin();
    }
  });
}

function forgetme() {
  lastloginDir = datapath + sep + "lastlogin.dat";
  if (fs.existsSync(lastloginDir)) {
    fs.unlinkSync(lastloginDir);
  }
}

function logout() {
  usersession = null;
  cloudIndex = undefined;
  transferInProgress = false;
  indexInProgress = false;
  localfiles = null;
  deleted = [];

  serverPub = null;
  sessionRSA = null;
  currentUser = undefined;
  messages = {};

  loggedIn = false;
  sessionUUID = null;
  sessionUsername = null;
  onMenuLogout();

  stopCloudInterval();
  openPage("login");
}

function finishLogin() {
  mainElement().innerHTML = "Loading from server...<br/><br/><img src='loading.gif'/>";
  onMenuLogin();
  getServerUserSession();
}

function sendFriendRequestTo(username) {
  if (!sessionRSA) return;
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=addOrRemFriend&action=request",
    type: 'POST',
    data: sessionRSA.encryptWithKey(username, serverPub),
    error: function(jqXHR, textStatus, errorThrown) {
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      handleUserSessionMessage(result);
    }
  });
}

function confirmFriendRequest(username) {
  if (!sessionRSA) return;
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=addOrRemFriend&action=confirm",
    type: 'POST',
    data: sessionRSA.encryptWithKey(username, serverPub),
    error: function(jqXHR, textStatus, errorThrown) {
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      handleUserSessionMessage(result);
    }
  });
}

function removeFriend(username) {
  if (!sessionRSA) return;
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=addOrRemFriend&action=remove",
    type: 'POST',
    data: sessionRSA.encryptWithKey(username, serverPub),
    error: function(jqXHR, textStatus, errorThrown) {
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      handleUserSessionMessage(result);
    }
  });
}

function handleUserSessionMessage(result) {
  usersession = JSON.parse(result);
  sessionUsername = usersession.username;
  serverUser = usersession.currentUser;
  flag = false;
  if (!currentUser || currentPage !== "chat") {
    flag = true;
  }
  if (serverUser && serverUser !== "null") {
    currentUser = serverUser;
    updateTextBox();  
  } else {
    flag = false;
    setUser("null");
    openPage("profile");
  }
  if (flag) {
    updateMessages();
    openPage("chat");
  }
  updateFriends();
  updateRequests();
}

function getServerUserSession() {
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=session",
    type: 'GET',
    error: function(jqXHR, textStatus, errorThrown) {
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      handleUserSessionMessage(result);
      $("#refreshButton").button('reset');
    }
  });
}

function updateFriends() {
  if (!loggedIn) {
    return;
  }
  if (currentPage === "chat" || currentPage === "profile") {
    if (typeof clearFriends === "function" && typeof addFriend === "function") {
      clearFriends();
      for (i = 0; i < usersession.contacts.length; i++) {
        addFriend(usersession.contacts[i]);   
      }
      updateProfilePics();
    }
  }
}

function setUser(setuserto) {
  console.log("setting user to " + setuserto);
  $.ajax({
    url: webserver + "command?session=" + sessionUUID + "&cmd=setuser",
    type: 'POST',
    data: sessionRSA.encryptWithKey(setuserto, serverPub),
    error: function(jqXHR, textStatus, errorThrown) {
      alertify.error("could not connect to server: " + errorThrown);
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      flag = false;
      if (!currentUser || currentPage !== "chat") {
        flag = true;
      }
      if (result && result === "200") {
        currentUser = setuserto;
        if (currentUser === "null"){
          openPage("profile");
          return;
        }
      } else {
        flag = false;
      }
      if (flag) {
        openPage("chat");
        updateMessages();
      } else {
        updateTextBox();
      }
    }
  });
}

function sendChatMessage(theForm) {
  if(theForm.msg.value === "") {
    return false;
  }
  text = theForm.msg.value;
  theForm.msg.value = "";
  sendMessage(text);
  return false;
}

function sendMessage(text) {
  if (usersession.contacts.indexOf(currentUser) !== -1) {
    $.ajax({
      url: webserver + "write?session=" + sessionUUID,
      type: 'POST',
      data: sessionRSA.encryptWithKey(text, serverPub),
      error: function(jqXHR, textStatus, errorThrown) {
        alertify.error("could not connect to server: " + errorThrown);
      },
      success: function(result) {
        if (!validSession()) {
          alertify.alert("invalid session");
          return;
        }
        if (!result || result !== "200") {
          alertify.alert("send message result " + result);
        }
      }
    });
  }
}

function updateMessages() {
  if (!currentUser) {
    alertify.error("no current user");
    return;
  }
  tmpuser = currentUser;
  $.ajax({
    url: webserver + "messages?session=" + sessionUUID,
    type: 'GET',
    error: function(jqXHR, textStatus, errorThrown) {
      if (loggedIn) {
        setTimeout(updateMessages, 1000);
      }
    },
    success: function(result) {
      if (!sessionRSA) return;
      result = sessionRSA.decryptWithOwnKey(result);
      if (!messages[tmpuser]) {
        messages[tmpuser] = "";
      }
      if (result && result !== " " && result !== "\n") {
        lines = result.split("\r");
        if (lines) {
          flag = false;
          for (i = 0; i < lines.length; i++) {
            if (lines[i].indexOf(":") > -1) {
              toadd = lines[i];
              messages[tmpuser] += toadd;
              if (i < lines.length - 1) {
                messages[tmpuser] += "\r";
              }
              flag = true;
            }
          }
          messages[tmpuser] += "\r";
          if (!currentUser || currentUser === "null") {
            return;
          }
          if (flag) {
            setTimeout(updateMessages, 100);
            if ($("#allmsgs") && $("#allmsgs")[0] && $("#allmsgs")[0].scrollHeight) {
              $("#allmsgs").animate({ scrollTop: $('#allmsgs')[0].scrollHeight}, 1000);
            }
          }
        }
      }
      updateTextBox();
    }
  });
}

function updateTextBox() {
  if (currentPage === "chat") {
    clearMessages();
    if (currentUser && usersession.contacts.indexOf(currentUser) === -1) {
      addCenter(currentUser + " has not accepted your friend request");
      return;
    }
    addCenter("Talking with: " + currentUser);
    if (currentUser && messages[currentUser]) {
      addLeftName = true;
      addRightName = true;
      lines = messages[currentUser].split("\r");

      for(var i = 0; i < lines.length; i++) {
        if (lines[i].startsWith(sessionUsername + ":")) {
          text = lines[i].substring(lines[i].indexOf(":") + 1, lines[i].length);
          addLeftName = true;
          if (addRightName) {
            addTopRight(sessionUsername);
            addRightName = false;
          }
          addRight(text);
        } else if (lines[i].startsWith(currentUser + ":")) {
          text = lines[i].substring(lines[i].indexOf(":") + 1, lines[i].length);
          addRightName = true;
          if (addLeftName) {
            addTopLeft(currentUser);
            addLeftName = false;
          }
          addLeft(text);
        } else if (lines[i].length !== 0) {
          addLeft(lines[i]);
        }
      }
    }
  }
}

function uploadProfilePicture(file) {
  if (!fs.existsSync(file)) {
    alertify.alert("file is invalid image or does not exist");
    return;
  }
  uploadCloudFile(file, "public/profile.png");
  updateProfilePics();
}

function openCloud() {
  sep = require("path").sep;
  require('nw.gui').Shell.openExternal(datapath + sep + "cloud" + sep + sessionUsername);
}
