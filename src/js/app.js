RSA = require("./js/rsaenc.js");
fs = require("file-system");
gui = require("nw.gui");

serverPublicKey = "";

sep = require("path").sep;
datapath = gui.App.dataPath;
pages = {};
loggedIn = false;

sessionUUID = null;
sessionUsername = null;
sessionRSA = null;

currentPage = null;

if (!fs.existsSync()) {
    fs.mkdirSync(datapath);
}

function validSession() {
    return (/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(sessionUUID));
}

function registerPage(name, func) {
    pages[name] = func;
}

function openPage(name) {
    $("#main").load("html/" + name + ".html", function() {
        currentPage = name;
        if (pages[name]) {
            pages[name]();
        }
    });
}

function readPage(url) {
    $.ajax({
        url: url,
        type: 'GET',
        error: function(jqXHR, textStatus, errorThrown) {
            return null;
        },
        success: function(result) {
            return result;
        }
    }
    );
}

function mainElement() {
    return document.getElementById("main");
}

$(document).ready(function() {
    if (debug) {
        openPage("login");
    } else {
        checkUpdates();
    }
});

function checkUpdates() {
  mainElement().innerHTML = "Checking for updates...<br/><br/><img src='loading.gif'/>";
  $.ajax({
    url: "http://isteinvids.co.uk/smalltalk/version",
    type: 'GET',
    error: function(jqXHR, textStatus, errorThrown) {
        alertify.alert("could not check updates");
        openPage("login");
    },
    success: function(result) {
        remoteVer = result;
        if (version === remoteVer) {
            openPage("login");
            return;
        }
        if (version > remoteVer) {
            console.log("client has newer version than remote?!");
            openPage("login");
            return;
        }
        if (version < remoteVer) {
            alertify.confirm("Your version is outdated, do you want to update?", function (ret) {
                if (ret) {
                    openPage("update");
                } else {
                    openPage("login");
                }
            });
            return;
        }
    }
});
}

registerPage("update", function() {
    download("http://isteinvids.co.uk/smalltalk/setup.exe");
});

function download(file_url){
    var fs = require('fs');
    var url = require('url');
    var http = require('http');

    var options = {
        host: url.parse(file_url).host,
        port: 80,
        path: url.parse(file_url).pathname
    };

    var file_name = url.parse(file_url).pathname.split('/').pop();
    var filep = datapath + sep + file_name;
    var file = fs.createWriteStream(filep);

    http.get(options, function(res) {
        var fsize = res.headers['content-length'];
        res.on('data', function(data) {
            file.write(data);
            progress(100-(((fsize-file.bytesWritten)/fsize)*100));
        }).on('end', function() {
            file.end();
            require('nw.gui').Shell.openExternal(filep);
            setTimeout(function() {
                process.exit(true);
            }, 2500);
        });
    });
}

function progress(percent) {
    setPercentage(Math.round(percent));
}
