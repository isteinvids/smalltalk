var os = require("os");
var gui = require('nw.gui');

opsy = os.type();

var win = gui.Window.get();
var tray = new gui.Tray({ icon: 'icon.png' });
tray.on('click', function() {win.show();});
// win.on('close', function() {this.hide();});

function onMenuLogin() {
    menuAddContact.enabled = true;
    menuLogout.enabled = true;
}

function onMenuLogout() {
    menuAddContact.enabled = false;
    menuLogout.enabled = false;
}

menuAddContact = new gui.MenuItem({
    label: 'Add Contact',
    enabled: false,
    click: function() {
        alertify.prompt("Add a contact", function (ret, str) {
            if (ret && str) {
                alertify.success("You've sent a friend request to " + str);
                sendFriendRequestTo(str);
            }
        }, "");
    }
});
menuLogout = new gui.MenuItem({
    label: 'Sign Out',
    enabled: false,
    click: function() {
        logout();
        forgetme();
    }
});
menuExit = new gui.MenuItem({
    label: 'Close',
    click: function() {
        process.exit(true);
    }
});

if (opsy === "Windows_NT") {
    var gui = require('nw.gui');

    var win = gui.Window.get();
    var menubar = new gui.Menu({
        type: 'menubar'
    });

    var file = new gui.Menu();
    file.append(menuLogout);
    file.append(menuExit);

    var contacts = new gui.Menu();
    contacts.append(menuAddContact);

/*    var subMenu = new gui.Menu();
    subMenu.append(new gui.MenuItem({
        label: 'SubMenu Action 1',
        click: function() {
            alert('SubMenu Action 1 Clicked');
        }
    }));
    subMenu.append(new gui.MenuItem({
        label: 'SubMenu Action 2',
        click: function() {
            alert('SubMenu Action 2 Clicked');
        }
    }));
file.append(new gui.MenuItem({ label: 'Sub Action Menu', submenu: subMenu}));*/

menubar.append(new gui.MenuItem({ label: 'SomeChat™', submenu: file}));
menubar.append(new gui.MenuItem({ label: 'Contacts', submenu: contacts}));

win.menu = menubar;
}

if (opsy === "Darwin") {
    var gui = require('nw.gui');

    // get the window object
    var win = gui.Window.get();

    var menubar = new gui.Menu({
        type: 'menubar'
    });

    var file = new gui.Menu();
    var subMenu = new gui.Menu();

    file.append(new gui.MenuItem({
        label: 'Action 1',
        click: function() {
            alert('Action 1 Clicked');
        }
    }));

    file.append(new gui.MenuItem({
        label: 'Action 2',
        click: function() {
            alert('Action 2 Clicked');
        }
    }));


    subMenu.append(new gui.MenuItem({
        label: 'SubMenu Action 1',
        click: function() {
            alert('SubMenu Action 1 Clicked');
        }
    }));

    subMenu.append(new gui.MenuItem({
        label: 'SubMenu Action 2',
        click: function() {
            alert('SubMenu Action 2 Clicked');
        }
    }));

    file.insert(new gui.MenuItem({
        label: 'Sub Action Menu',
        submenu: subMenu
    }));

    win.menu = menubar;
    win.menu.insert(new gui.MenuItem({
        label: 'SomeChat™',
        submenu: file
    }), 1);
}
