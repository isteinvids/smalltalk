var crypto = require('crypto');
var fs = require('file-system');

var Reader = function(dir, empt, flg) {
  this.directory = dir;
  this.files = fs.readdirSync(this.directory);
  this.toret = {};
  this.absdir = "";
  this.empty = empt;
  this.flag = flg;
  if (flg === undefined) {
    this.flag = false;
  }
}

Reader.prototype.readdir = function() {
  for (this.i = 0; this.i < this.files.length; this.i++) {
    this.absdir = this.directory + this.files[this.i];
    this.stats = fs.lstatSync(this.absdir);
    if (this.stats.isDirectory()) {
      this.absdir = this.directory + this.files[this.i] + "/";
      this.tempreader = new Reader(this.absdir, this.empty, this.flag);
      this.toret[this.absdir] = (this.tempreader.readdir(this.absdir));
      if (this.empty) {
        this.flag = this.tempreader.flag;
      }
    } else {
      if (this.empty) {
        this.flag = true;
      } else {
        this.absdir = this.directory + this.files[this.i];
        this.toret[this.absdir] = {"md5" : "undefined", "mtime" : (this.stats.mtime / 1000 | 0)};
      }
    }
  }
  if (this.empty) {
   return !this.flag;
 }
 return this.toret;
}

module.exports = {
  readdir: function(dir) {
    return new Reader(dir, false).readdir();
  },
  dirempty: function(dir) {
    return new Reader(dir, true).readdir();
  },
  encryptmd5: function(text) {
    return crypto.createHash('md5').update(text).digest('hex');
  },
  split: function(str, separator, limit) {
    str = str.split(separator);

    if (str.length > limit) {
      var ret = str.splice(0, limit);
      ret.push(str.join(separator));

      return ret;
    }

    return str;
  },
  generateString: function(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },
  readmd5: function(file, callback) {
    if (!fs.existsSync(file)) {
      callback(undefined, undefined);
      return;
    }
    var algo = 'md5';
    var shasum = crypto.createHash(algo);

    var s = fs.ReadStream(file);
    s.on('data', function(d) { shasum.update(d); });
    s.on('end', function() {
      var md5 = shasum.digest('hex');
      callback(undefined, md5);
    });
  },
  generateUUID: function() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
    }
    return (s4() + s4() + '-' + s4() + '-' + s4() + '-' +
     s4() + '-' + s4() + s4() + s4());
  },
  encode: function(text) {
    return encodeURIComponent(text).replace(/'/g,"%27").replace(/"/g,"%22");  
  },
  decode: function(text) {
    return decodeURIComponent(text.replace(/\+/g,  " "));
  }
};
